from abc import ABCMeta, abstractmethod
import random, time

import logging

from core.helpers import (
    Observer, Singleton,
    strict_argument_types, strict_return_type
)

logging.basicConfig(format = u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s %(message)s', level = logging.DEBUG)


class Status(metaclass=ABCMeta):

    def __init__(self, period):
        self.period = period

    def progress(self, left):
        if self.period >= left:
            self.period -= left
        else:
            self.ready()

    @abstractmethod
    def ready(self):
        """"""


class Skill(metaclass=ABCMeta):

    def __init__(self, name, subject):
        self.name = name
        self.subject = subject

    def do(self):
        for s in self.subject._statuses:
            if isinstance(s, Work):
                s.company.made()


class Human(Observer):

    _population = []
    _total_count = 0

    def __init__(self, name, mother, father, sex, age=0, dna=None):
        self.name = name
        self.mother = mother
        self.father = father
        self.age = age
        self.sex = sex
        self.food = None
        self.dna = dna or self.get_dna()
        self._statuses = []
        self._skills = {}
        self._money = 0

    def handle_event(self, value):
        self.age += value
        for status in self._statuses:
            status.progress(value)
            self.find_food()

        for skill in self._skills.values():
            skill.do()

    def get_dna(self):
        if isinstance(self.mother, Human):
            self.dna = self.mother.dna + self.father.dna
        else:
            self.dna = ''
            for x in range(10):
                self.dna += random.choice(list('AGTC'))

    def statuses(self):
        return self._statuses

    @staticmethod
    def sexloto():
        return random.choice(['m', 'w'])

    @strict_argument_types
    def set_status(self, status:Status):
        self._statuses.append(status)

    @strict_argument_types
    def add_skill(self, skill:Skill):
        self._skills[skill.name] = skill

    @strict_argument_types
    def give_money(self, quantity:int):
        self._money += quantity

    @classmethod
    def population_grow(cls, new_born=None):
        if new_born:
            cls._population.append(new_born)
        else:
            cls._population.append(cls())
        cls._total_count += 1

    @classmethod
    def find_money(cls):
        """"""

    # @classmethod
    def find_food(self):
        food = self.food
        if self._money:
            food_shop = Manufactures.get_lower_price_for(food)
            food_shop.sell(product=food,
                           coast=food_shop.get_product_info(food, 'price'),
                           quantity=1)
        else:
            self.find_money()

    @classmethod
    def get_population(cls):
        return cls._population[:]

    @classmethod
    def get_total_count(cls):
        return cls._total_count


class Work(Status):
    def __init__(self, subject:Human, company:"Company", position:"Position", period:int=1):
        super().__init__(period)
        self.period = period
        self.company = company
        self.position = position
        self.subject = subject

    def ready(self):
        self.subject.give_money(
            self.company.pay(
                self.position.get_payment()
            )
        )


class Pregnant(Status):
    def __init__(self, subject:Human, parent2:Human, period:int=9):
        self.period = period
        self.parent2 = parent2
        self.subject = subject
        super(Pregnant, self).__init__(self.period)

    @staticmethod
    def born(mother:Human, father:Human) -> Human:
        new_born = Human(mother.name+ "'s & " + father.name +"'s child",
                         (mother, father),
                         Human.sexloto(),
                         mother.dna + father.dna)
        Human.population_grow(new_born)
        return new_born

    def ready(self):
        self.born(self.subject, self.parent2)


class Position(object):
    def __init__(self):
        self._payment = 0
        self._stuff = []
        self._required_skills = []

    def set_payment(self, new_payment):
        self._payment = new_payment

    def get_payment(self):
        return self._payment

    def new_member(self, member):
        self._stuff.append(member)

    def bye_member(self, member):
        if member in self._stuff:
            return self._stuff.pop(
                self._stuff.index(member)
            )

    def add_required_skill(self, skill:Skill):
        self._required_skills.append(skill)

class Company(Observer):
    """
    Класс "работа", она же фирма и т.д.
    """
    def __init__(self):
        self._balance = 0
        self._power = 0          # произв. мощность
        self._structure = {}     # словарь должностей.
        self._products = {}

    def handle_event(self, subject):
        # for position in self._structure:
        #     for worker in position._stuff:
        #         for skill in position._required_skills:
        #             printt(skill)
        #             worker._skills[skill.name].do()
        pass
        # self.made()

    def hire(self, position, empl:Human):
        """
        метод найм сотрудников
        """
        if self._structure.get(position):
            self._structure[position].append(empl)
            work = Work(empl, self, position)
            empl.set_status(work)
            self._power += 1
        else:
            self._structure[position] = [empl]
            self._power += 1

    def fire(self, empl:Human, position=None):
        """
        метод увольнение
        """
        if position and self._structure.get(position):
            self._structure[position].bye_member(empl)
            self._power -= 1
        else:
            """ 
            на сучай если должность не известна
            или просто не указана
            """
            fired = None
            for position in self._structure.keys():
                if not fired:
                    fired = self._structure[position].bye_member(empl)
                    self._power -= 1
                else:
                    break

    def add_position(self, position):
        self._structure[position] = None

    @property
    def balance(self):
        return self._balance

    @balance.setter
    @strict_argument_types
    def balance(self, value:int):
        self._balance += value

    def get_products(self):
        return self._products.copy()

    def get_products_list(self):
        return self._products.keys()

    def get_product_info(self, product: 'Product', info:str='price'):
        """
        Получить инфу о продукте
        """
        return self.get_products().get(product).get(info)

    @strict_argument_types
    def pay(self, coast:int) -> int:
        if self._balance >= coast:
            self._balance -= coast
            logging.info(
                'PAY {} by {}. Current balance: {}'.format(
                        coast,
                        self,
                        self._balance))
            return coast

        msg = "{} try to pay, but can't..."
        logging.info(msg.format(self))
        return 0

    def buy(self, company:"Company", product:"Product", quantity:int):
        price = company.get_product_info(product, 'price')
        if price and price * quantity <= self._balance:
            logging.info(
                'BUY {} in {}. Current balance: {}'.format(
                        product,
                        company,
                        self._balance))
            return company.sell(
                product,
                self.pay(price),
                quantity
            )
        else:
            print(price, quantity, self._balance)
            logging.info('$$$ low balance or wrong price $$$')

    def add_product(self, product:"Product", price:int=1, quantity=0,):
        self._products[product] = {
            "quantity":quantity,
            "price":price,
        }

    def set_product_price(self, product:"Product", new_price):
        self._products.get(product)['price'] = new_price

    def made(self, product:"Product"=None, quantity:int=1):
        if not product:
            product = list(self._products.keys())[0]

        if self.get_products().get(product):

            for source in product.get_sources():
                s_quantity = list(source.values())[0]
                source = list(source.keys())[0]

                seller = Manufactures().get_lower_price_for(source)
                source = self.buy(seller, source, s_quantity)

                if source: continue
                else:
                    msg = "{} try to buy {}, but can't..."
                    logging.info(msg.format(self, product.get_sources()))

            while quantity:
                if self._power >= quantity:
                    self._products[product]['quantity'] += quantity
                    break
                else:
                    self._products[product]['quantity'] += self._power
                    quantity -= self._power

            logging.info('{} {}`s was MADE on {}'.format(quantity,
                                                          product,
                                                          self))

    def sell(self, product:"Product", coast:int, quantity:int):
        if self._products.get(product) \
            and self._products[product]['quantity'] >= quantity and \
                coast <= self._products[product]["price"]:
            self.balance =coast
            self._products[product]['quantity'] -= quantity

            logging.info('{} {}`s was SOLD on {}'.format(quantity,
                                                         product,
                                                         self))

        elif self._products.get(product):
            self.made(product, quantity)    # если нечего продать, то производим

            self.sell(                      # и сразу продаём
                product,
                self.get_products().get(product)['price'],
                quantity
            )
        else:
            print('trying to buy wrong product')

        return quantity


class Product(metaclass=ABCMeta):
    def __init__(self):
        self._sources = []

    @strict_argument_types
    def add_sources(self, source:"Product", quantity:int):
        assert isinstance(source, Product)
        self._sources.append({source:quantity})

    def get_sources(self):
        return self._sources


class Manufactures(Singleton):

    _companies = {}

    @classmethod
    def register(cls, company:Company):
        for product in company.get_products_list():
            price = company.get_products().get(product)['price']

            if cls._companies.get(product):
                cls._companies[product].append({
                    'company':company,
                    'price':price
                })
            else:
                cls._companies[product] = [{'company':company,
                                             'price':price}]
                logging.info('Product {} registred'.format(product))
        logging.info('COMPANY {} REGISTRED'.format(company))

    @classmethod
    def get_lower_price_for(cls, product):
        return sorted(
            cls._companies.get(product), key=lambda c: c['price']
        )[0]['company']


class Chronos(Singleton):

    __observers = []
    __play = False

    @classmethod
    def get_observers(cls):
        return cls.__observers

    @classmethod
    def add_observer(cls, observer):
        assert isinstance(observer, Observer)
        cls.__observers.append(observer)

    @classmethod
    def del_observer(cls, observer):
        if observer in cls.observers:
            cls.__observers.remove(observer)

    @classmethod
    def notify_observers(cls):
        for observer in cls.__observers:
            observer.handle_event(cls)

    @classmethod
    def move_time(cls, step=1, pause=1):
        cls.__play = True

        while cls.__play:
            for o in cls.get_observers():
                o.handle_event(step)

            everybodies_counts = [h._money for h in Human.get_population()]
            happynes = sum(everybodies_counts) / \
                       len(Human.get_population())

            max_maney = max(everybodies_counts)
            min_money = min(everybodies_counts)

            print('все счета:', everybodies_counts)
            print('средний достаток:', happynes)
            print('самый богатый:', max_maney)
            print('самый бедный:', min_money)

            time.sleep(pause)

    @classmethod
    def time_stop(cls):
        cls.__play = False

    observers = property(get_observers, add_observer, del_observer)


