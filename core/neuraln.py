import numpy as np


class NeuralNetwork(object):
    def __init__(self):
        self.inputLayerSize = 2  # Количество входов (зависимые пары)
        self.outPutLayerSize = 1
        self.hiddenLayerSize = 3

        self.W1 = np.random.randn(self.inputLayerSize, \
                                  self.hiddenLayerSize)

        self.W2 = np.random.randn(self.hiddenLayerSize, \
                                  self.outPutLayerSize)

    def forward(self, X):

        self.z2 = np.dot(X, self.W1)        # произведение сумм весов первого слоя и синапсов
        self.a2 = self.sigmoid(self.z2)     # выход из нейрона после сигмоида. По z2.
        self.z3 = np.dot(self.a2, self.W2)  # произведение сумм весов второго слоя и синапсов
        yHat = self.sigmoid(self.z3)        # решение сети
        return yHat

    @staticmethod
    def sigmoid(z):
        return 1 / (1 + np.exp(-z))
