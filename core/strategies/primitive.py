from core import (
    Human, Skill,
    Product, Position,
    Company, Manufactures,
    Chronos
)

from core.strategies.skills import Cooking, Ferming


def touch(mans, womans):

    while mans:
        h = Human('A' + str(Human.get_total_count()), ('God', 'Univers'), 0, 'm')
        Human.population_grow(h)

        ferming = Ferming('ferming',h)
        h.add_skill(ferming)
        mans -= 1

    while womans:
        h = Human('A' + str(Human.get_total_count()), ('God', 'Univers'), 0, 'w')
        Human.population_grow(h)

        cooking = Cooking('cooking',h)
        h.add_skill(cooking)
        womans -= 1


food = Product()
potato = Product()

food.add_sources(potato, quantity=1)

cook = Position()
cook.set_payment(.1)
cook.add_required_skill(Cooking)

cowboy = Position()
cowboy.set_payment(.1)
cowboy.add_required_skill(Ferming)

ferm = Company()
ferm.add_product(potato, 1, 1)
ferm.add_position(cowboy)
Chronos.add_observer(ferm)
Manufactures.register(ferm)

feed = Company()
feed.add_product(food, 1.1, 1)
feed.add_position(cook)
Chronos.add_observer(feed)
Manufactures.register(feed)


touch(6, 6)

population = Human.get_population()

for p in population:
    p.food = food
    Chronos.add_observer(p)

for man in population[:int(len(population)/2)]:
    feed.hire(
        cook,
        population.pop()
    )
    print('cook')

for man in population:
    ferm.hire(
        cowboy,
        man
    )
    print('cowboy')

feed.balance = 10
ferm.balance = 10


def main():
    Chronos.move_time(4, 4)
