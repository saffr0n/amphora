"""
Файл со вспомогательными классами и методами
"""

from abc import ABCMeta, abstractmethod
from inspect import signature
from functools import wraps


def strict_argument_types(func):
    """
    Декоратор проверяющий соответствие
    типов аргументов поданных в функцию ожидаемым.
    В случае не соответствия возбуждается ошибка ValueError
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        f = signature(func)
        for p, a in zip(f.parameters, args):
            # print(f.parameters)
            # print(f.parameters[p])
            # print(f.parameters[p].annotation)
            annotation = f.parameters[p].annotation
            real_type = type(a)
            if annotation != "_empty" and not isinstance(a, real_type):
                err = 'The argument "{}" must be "{}", passed "{}"'
                raise ValueError(err.format(a, annotation.__name__, real_type.__name__ ))
        return func(*args, **kwargs)
    return wrapper


def strict_return_type(func):
    """
    Декоратор проверяющий соответствие типа
    возвращаемого функцией значения ожидаемому.
    В случае не соответствия возбуждается ошибка ValueError
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        f = signature(func)
        annotation = f.return_annotation
        real_type = func(*args, **kwargs)
        if annotation != "_empty" and not isinstance(real_type, annotation):
            err = 'The return value must be "{}", not "{}"'
            raise ValueError(err.format(annotation.__name__, type(real_type).__name__ ))
        return func(*args, **kwargs)
    return wrapper


# Observer - Наблюдатель

class Subject(metaclass=ABCMeta):
    """

    """
    def __init__(self):
        self.__observers = []

    @strict_argument_types
    def add_observer(self, observer:"Observer"):
        assert isinstance(observer, Observer)
        self.__observers.append(observer)

    @strict_argument_types
    def remove_observer(self, observer:"Observer"):
        if observer in self.__observers:
            self.__observers.remove(observer)

    def notify_observers(self):
        for observer in self.__observers:
            observer.handle_event(self)


class Observer(metaclass=ABCMeta):
    @abstractmethod
    def handle_event(self, subject):
        pass


class Singleton(object):
    __instance = None

    @classmethod
    def get_instance(cls):
        if cls.__instance is None:
            cls.__instance = cls()
        return cls.__instance
